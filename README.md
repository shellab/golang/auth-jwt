# auth-jwt

## Environment

 - PORT=7788
 - LOG_LEVEL=TRACE
 - LOG_PATH=./log/server.log
 - DB_HOST=
 - DB_USER=
 - DB_PASSWD=
 - DB_NAME=
 - JWT_SECRET=

## TODO

 - black list of revoke tokens in redis when it's logout.
