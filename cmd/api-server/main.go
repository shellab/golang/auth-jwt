package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/joho/godotenv/autoload"
	log "github.com/sirupsen/logrus"

	//"auth-jwt/pkg/database"
	_ "auth-jwt/pkg/logger"
	"auth-jwt/pkg/server"
)

const (
	SHUTDOWN_TIMEOUT = 3
)

func main() {
	port := fmt.Sprintf(":%s", os.Getenv("PORT"))
	//database.GormAutoMigrate()
	apiServer := server.NewServer(port)

	sigs := make(chan os.Signal)
	signal.Notify(sigs,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGKILL,
	)

	go func() {
		if err := apiServer.ListenAndServe(); err != nil {
			if errors.Is(err, http.ErrServerClosed) {
				log.Info("Server graceful shutdown.")
			} else {
				log.Fatalln("Server shutdown error:", err)
			}
		}
	}()

	// wait for signal, start to shutdown api server
	<-sigs
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(SHUTDOWN_TIMEOUT)*time.Second)
	// context 3 秒結束，或是當 cancel 呼叫時立刻結束
	defer cancel()

	if err := apiServer.Shutdown(ctx); err != nil {
		log.Fatalln("Shut down http server action error.", err)
	}

	// wait for server shutdown finish.
	<-ctx.Done() // 等 context 完成，最多 3 秒，或是 cancel 被呼叫

	log.Info("api server exist successful.")
}
