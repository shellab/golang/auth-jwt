package server

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"auth-jwt/pkg/routes"
)

func NewServer(addr string) *http.Server {
	ginEngine := gin.Default()

	routes.Bind(ginEngine)

	return &http.Server{
		Addr: addr,
		Handler: ginEngine,
	}
}
