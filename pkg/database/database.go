package database

import (
	"fmt"
	"os"
	"sync"

	log "github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"auth-jwt/pkg/models"
)

var once sync.Once
var singletonDB *gorm.DB

func GetDB() *gorm.DB {
	once.Do(func() {
		dsn := fmt.Sprintf("host=%s user=%s password=%s port=%d dbname=%s",
			os.Getenv("DB_HOST"),
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASSWD"),
			5432,
			os.Getenv("DB_NAME"),
		)
		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
			DisableForeignKeyConstraintWhenMigrating: true,
		})
		if err != nil {
			log.Fatalln("Can't connect to database with DSN:", dsn)
		}
		singletonDB = db.Debug()
	})
	return singletonDB
}

func GormAutoMigrate() {
	db := GetDB()
	if err := db.AutoMigrate(
		&models.User{},
		&models.Token{},
	); err != nil {
		log.Fatalln("database migrate failed with error:", err)
	}
}
