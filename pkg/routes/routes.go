package routes

import (
	"github.com/gin-gonic/gin"

	"auth-jwt/pkg/controllers"
	"auth-jwt/pkg/middleware"
)

func Bind(ginEngine *gin.Engine) {
	api := ginEngine.Group("api")
	v1 := api.Group("v1")

	systemController := controllers.NewSystemController()
	system := v1.Group("system")
	system.GET("/health", systemController.HealthCheck)

	// auth
	authenticationController := controllers.NewAuthenticationController()
	ginEngine.POST("/register", authenticationController.Register)
	ginEngine.POST("/login", authenticationController.Login)
	ginEngine.POST("/logout", middleware.TokenAuthenticate, authenticationController.Logout)
	ginEngine.POST("/refresh", authenticationController.TokenRenew)

	// user
	userController := controllers.NewUserController()
	user := v1.Group("")
	user.Use(middleware.TokenAuthenticate)
	user.GET("/users", userController.GetUserList)
	user.GET("/user/:id", userController.GetUser)
}
