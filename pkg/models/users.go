package models

import (
	"time"

	"gorm.io/gorm"
)

/*
CREATE TABLE "users" (
	"id" bigserial,
	"created_at" timestamptz,
	"updated_at" timestamptz,
	"deleted_at" timestamptz,
	"email" text,
	"name" text,
	"password" text,
	"last_login_at" timestamptz DEFAULT null,
	PRIMARY KEY ("id")
)
*/
type User struct {
	gorm.Model
	Email       string
	Name        string
	Password    string
	LastLoginAt *time.Time // zero-value = nil 寫入時會是 null
	Tokens      []Token    // has many tokens
}

/*
	- 分成 zero-value, null, default value (比較用不到，由程式自己處理)
	- 寫入 db 如果可以是 null (無) 那要設定該欄位為 pointer ex: LastLoginAt *time.Time
	- 所有不是 pointer type 的欄位寫入 DB 都是 zero-value
		- string -> ""
		- int -> 0
		- bool -> false
		- time.Time -> 1900-01-01
	- 避免 zero-value 而改用 default value 的話要設定 gorm:"default:N/A" (default 由app 自己控制比較好)
*/
