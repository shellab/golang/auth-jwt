package models

import "gorm.io/gorm"

type Token struct {
	gorm.Model
	UserID       uint
	RefreshToken string `gorm:"not null"`
}
