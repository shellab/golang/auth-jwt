package authenticate

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) string {
	hashPass, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(hashPass)
}

func VerifyPassword(userProvidePass string, dbRecordPass string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(dbRecordPass), []byte(userProvidePass)); err != nil {
		return false
	}
	return true
}

func VerifyToken(token string, secret string) (*SignedClaims, error) {
	parsedToken, err := jwt.ParseWithClaims(token, &SignedClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(secret), nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := parsedToken.Claims.(*SignedClaims)
	if !ok {
		return nil, fmt.Errorf("invalid claims")
	}

	return claims, nil
}

type SignedClaims struct {
	Email string
	Name  string
	jwt.StandardClaims
}

type JWT struct {
	secret []byte
	claim  SignedClaims
}

func NewJWT(email string, name string, secret string) *JWT {
	return &JWT{
		secret: []byte(secret),
		claim: SignedClaims{
			Email: email,
			Name:  name,
			StandardClaims: jwt.StandardClaims{
				Issuer: "shellab",
			},
		},
	}
}

func (j *JWT) GenerateAccessToken() (string, error) {
	now := time.Now()
	j.claim.Subject = "access token"
	j.claim.IssuedAt = now.Unix()
	j.claim.ExpiresAt = now.Add(time.Minute * time.Duration(15)).Unix() // 15m

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, j.claim)
	accessToken, err := token.SignedString(j.secret)
	if err != nil {
		return "", err
	}
	return accessToken, nil
}

func (j *JWT) GenerateRefreshToken() (string, error) {
	now := time.Now()
	j.claim.Subject = "refresh token"
	j.claim.IssuedAt = now.Unix()
	j.claim.ExpiresAt = now.Add(time.Hour * time.Duration(24*90)).Unix() // 90 days

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, j.claim)
	refershToken, err := token.SignedString(j.secret)
	if err != nil {
		return "", err
	}
	return refershToken, nil
}
