package middleware

import (
	"auth-jwt/pkg/authenticate"
	"auth-jwt/pkg/cache"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/redis/go-redis/v9"
)

func TokenAuthenticate(ctx *gin.Context) {
	authHeader := ctx.Request.Header.Get("Authorization")
	if authHeader == "" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"error": "No Authorization header provided",
		})
		return
	}

	authHeaderParts := strings.Split(authHeader, " ")
	if len(authHeaderParts) != 2 || authHeaderParts[0] != "Bearer" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"error": "Not a Bearer token",
		})
		return
	}

	accessToken := authHeaderParts[1]
	claims, err := authenticate.VerifyToken(accessToken, os.Getenv("JWT_SECRET"))
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"error": err.Error(),
		})
		return
	}

	if claims.Subject != "access token" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"error": "Not a access token.",
		})
		return
	}

	// is revoke token?
	cacheClient := cache.GetCache()
	val, err := cacheClient.Get(ctx, fmt.Sprintf("%s:%s:%s", "auth-jwt", "access-token", accessToken)).Result()
	if err != nil && err != redis.Nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": "check token error.",
		})
		return
	}

	if val != "" { // if key exist, val should be "email"
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"error": "token has been revoked.",
		})
		return
	}

	// store account info into context
	ctx.Set("token", accessToken)
	ctx.Set("claim", claims)
	ctx.Next()
}
