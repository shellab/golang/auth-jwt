package logger

import (
	"fmt"
	"io"
	"os"

	log "github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
)

func init() {
	switch logLevel := os.Getenv("LOG_LEVEL"); logLevel {
	case "TRACE":
		log.SetLevel(log.TraceLevel)
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
	case "INFO":
		log.SetLevel(log.InfoLevel)
	case "WARNING":
		log.SetLevel(log.WarnLevel)
	case "ERROR":
		log.SetLevel(log.ErrorLevel)
	case "FATAL":
		log.SetLevel(log.FatalLevel)
	case "PANIC":
		log.SetLevel(log.PanicLevel)
	default:
		log.SetLevel(log.InfoLevel)
	}

	logPath := os.Getenv("LOG_PATH")
	fmt.Println(logPath)
	if logPath == "" {
		log.Fatalln("Log path is empty.")
	}

	/* create file to log
	f, err := os.OpenFile(logPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, os.FileMode(0644))
	if err != nil {
		log.Fatalln("Can not open log file.", logPath)
	}
	*/

	logRotate := &lumberjack.Logger{
		Filename:   logPath,
		MaxSize:    30, // megabytes
		MaxBackups: 100,
		MaxAge:     365,  //days
		Compress:   true, // disabled by default
	}

	log.SetOutput(io.MultiWriter(logRotate, os.Stdout))
	log.Info("Logger has been initiated.")
}
