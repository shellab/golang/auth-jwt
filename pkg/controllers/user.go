package controllers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"auth-jwt/pkg/dao"
)

type UserController struct {
}

func NewUserController() *UserController {
	return &UserController{}
}

func (uc *UserController) GetUserList(ctx *gin.Context) {
	users, err := dao.GetUserList()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, users)
}

func (uc *UserController) GetUser(ctx *gin.Context) {
	userID := ctx.Param("id")
	uintUserID, _ := strconv.ParseUint(userID, 10, 32)
	daoUser, err := dao.NewUser(uint(uintUserID))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, daoUser.GetData())
}
