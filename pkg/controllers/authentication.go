package controllers

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"auth-jwt/pkg/authenticate"
	"auth-jwt/pkg/cache"
	"auth-jwt/pkg/dao"

	log "github.com/sirupsen/logrus"

	"github.com/gin-gonic/gin"
)

type AuthenticationController struct {
}

// ------ request  ------
type registeUserRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Name     string `json:"name" binding:"required,min=3"`
	Password string `json:"password" binding:"required,min=8,max=20"`
}

type loginRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
}

type refreshRequest struct {
	RefreshToken string `json:"refresh_token" binding:"required"`
}

// ------ response ------
type registeUserResponse struct {
	UserID uint `json:"user_id"`
}

type loginResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type refreshResponse struct {
	AccessToken string `json:"access_token"`
}

func NewAuthenticationController() *AuthenticationController {
	return &AuthenticationController{}
}

func (ac *AuthenticationController) Register(ctx *gin.Context) {
	req := registeUserRequest{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	userID, err := dao.CreateUser(req.Email, req.Name, req.Password)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, registeUserResponse{
		UserID: userID,
	})
}

func (ac *AuthenticationController) Login(ctx *gin.Context) {
	// validate user information
	req := loginRequest{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	daoUser, err := dao.NewUserByEmail(req.Email)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}
	if !daoUser.Validation(req.Password) {
		// invalidate user
		ctx.JSON(http.StatusBadRequest, requestErrorResponse{
			Message: "password is incorrect",
		})
		return
	}
	// generate access token and refresh token
	userDetail := daoUser.GetData()
	jwt := authenticate.NewJWT(userDetail.Email, userDetail.Name, os.Getenv("JWT_SECRET"))
	accessToken, accessTokenErr := jwt.GenerateAccessToken()
	refreshToken, refreshTokenErr := jwt.GenerateRefreshToken()

	if accessTokenErr != nil || refreshTokenErr != nil {
		ctx.JSON(http.StatusInternalServerError, requestErrorResponse{
			Message: "token generate failed.",
		})
		return
	}

	// record to DB, last_login_at and write refresh token to user tokens
	err = daoUser.UpdateLogin(refreshToken)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, loginResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	})
}

func (ac *AuthenticationController) Logout(ctx *gin.Context) {
	claim, _ := ctx.Get("claim")
	token, _ := ctx.Get("token")

	authClaim := claim.(*authenticate.SignedClaims)
	now := time.Now().Unix()
	blockRemainDuration := authClaim.ExpiresAt - now

	cacheClient := cache.GetCache()
	err := cacheClient.Set(context.Background(),
		fmt.Sprintf("%s:%s:%s", "auth-jwt", "access-token", token),
		authClaim.Email,
		time.Second*time.Duration(blockRemainDuration),
	).Err()
	if err != nil {
		log.Info("revoke failed with token:", token)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Status(http.StatusOK)
}

func (ac *AuthenticationController) TokenRenew(ctx *gin.Context) {
	req := refreshRequest{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	claim, err := authenticate.VerifyToken(req.RefreshToken, os.Getenv("JWT_SECRET"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	// check refresh token still exist
	daoUser, err := dao.NewUserByEmail(claim.Email)
	if err != nil {
		ctx.JSON(http.StatusNotFound, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	if ok := daoUser.HasRefreshToken(req.RefreshToken); !ok {
		ctx.JSON(http.StatusNotFound, requestErrorResponse{
			Message: "refresh token not exist, please login",
		})
		return
	}

	// generate new access token
	userData := daoUser.GetData()
	jwt := authenticate.NewJWT(userData.Email, userData.Name, os.Getenv("JWT_SECRET"))
	newAccessToken, err := jwt.GenerateAccessToken()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, requestErrorResponse{
			Message: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, refreshResponse{
		AccessToken: newAccessToken,
	})
}
