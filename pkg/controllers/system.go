package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type SystemController struct {
}

func NewSystemController() *SystemController {
	return &SystemController{}
}

func (sc *SystemController) HealthCheck(c *gin.Context) {
	c.String(http.StatusOK, "ok")
}
