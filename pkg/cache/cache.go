package cache

import (
	"context"
	"os"
	"sync"

	log "github.com/sirupsen/logrus"
	"github.com/redis/go-redis/v9"
)

var once sync.Once
var singletonCache *redis.Client

func GetCache() *redis.Client {
	once.Do(func(){
		client := redis.NewClient(&redis.Options{
			Addr: os.Getenv("REDIS_HOST"),
			Password: os.Getenv("REDIS_PASSWD"),
			DB: 0,
		})
		_, err := client.Ping(context.Background()).Result()
		if err != nil {
			log.Fatalln("Can't connect to redis.")
		}
		singletonCache = client
	})
	return singletonCache
}
