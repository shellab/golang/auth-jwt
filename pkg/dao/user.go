package dao

import (
	"errors"
	"fmt"
	"time"

	"github.com/jinzhu/copier"
	"gorm.io/gorm"

	"auth-jwt/pkg/authenticate"
	"auth-jwt/pkg/database"
	"auth-jwt/pkg/models"
)

type UserSummary struct {
	ID    uint   `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

type UserDetail struct {
	UserSummary
	FormatCreatedAt   string `json:"created_at"`
	FormatUpdatedAt   string `json:"updated_at"`
	FormatLastLoginAt string `json:"last_login_at"`
}

func (u *UserDetail) CreatedAt(t time.Time) { // user copier Copy to method https://github.com/jinzhu/copier
	u.FormatCreatedAt = t.Format(time.RFC3339)
}

func (u *UserDetail) UpdatedAt(t time.Time) {
	u.FormatUpdatedAt = t.Format(time.RFC3339)
}

func (u *UserDetail) LastLoginAt(t *time.Time) {
	if t != nil {
		u.FormatLastLoginAt = t.Format(time.RFC3339)
	}
}

type User struct {
	db        *gorm.DB
	modelUser models.User
}

func NewUser(id uint) (*User, error) {
	db := database.GetDB()
	modelUser := models.User{}
	r := db.Where("id = ?", id).
		Scopes(preloadToken).
		Take(&modelUser)
	if r.Error != nil {
		return nil, r.Error
	}

	return &User{
		db:        db,
		modelUser: modelUser,
	}, nil
}

func NewUserByEmail(email string) (*User, error) {
	db := database.GetDB()
	modelUser := models.User{}
	r := db.Where("email = ?", email).
		Scopes(preloadToken).
		Take(&modelUser)
	if r.Error != nil {
		return nil, r.Error
	}

	return &User{
		db:        db,
		modelUser: modelUser,
	}, nil
}

func (u *User) Validation(password string) bool {
	return authenticate.VerifyPassword(password, u.modelUser.Password)
}

func (u *User) GetData() UserDetail {
	userDetail := UserDetail{}
	copier.Copy(&userDetail, &u.modelUser)

	return userDetail
}

func (u *User) HasRefreshToken(refreshToken string) bool {
	for _, token := range u.modelUser.Tokens {
		if refreshToken == token.RefreshToken {
			return true
		}
	}
	return false
}

func (u *User) UpdateLogin(refreshToken string) error {
	now := time.Now()
	if len(u.modelUser.Tokens) >= 3 { // if token more than 3, replace the oldest
		u.modelUser.Tokens[0].RefreshToken = refreshToken
		u.modelUser.Tokens[0].CreatedAt = time.Now()
		u.db.Save(&u.modelUser.Tokens[0]) // update token record.
	} else {
		u.modelUser.Tokens = append(u.modelUser.Tokens, models.Token{
			RefreshToken: refreshToken,
		}) // use save() to upsert save.
	}
	u.modelUser.LastLoginAt = &now // update user last login time
	r := u.db.Save(&u.modelUser)
	if r.Error != nil {
		return r.Error
	}
	return nil
}

func GetUserList() ([]UserSummary, error) {
	db := database.GetDB()

	users := []UserSummary{}
	r := db.Model(&models.User{}).
		Select(
			"id",
			"name",
			"email").
		Find(&users)
	if r.Error != nil {
		return users, r.Error
	}
	return users, nil
}

func CreateUser(email string, name string, password string) (uint, error) {
	db := database.GetDB()
	// check email exist
	r := db.Where("email = ?", email).Take(&models.User{})

	var user models.User
	if r.Error != nil {
		if errors.Is(r.Error, gorm.ErrRecordNotFound) {
			// user not exist, create a new user
			user = models.User{
				Email:    email,
				Name:     name,
				Password: authenticate.HashPassword(password),
			}
			r := db.Create(&user)
			if r.Error != nil {
				return 0, fmt.Errorf("create user error: %s", r.Error)
			}
		} else {
			// unknow error
			return 0, fmt.Errorf("check user error: %s", r.Error)
		}
	} else {
		// user exist
		return 0, fmt.Errorf("user exist with email: %s", email)
	}
	return user.ID, nil
}

func preloadToken(db *gorm.DB) *gorm.DB {
	return db.Preload("Tokens", func(db *gorm.DB) *gorm.DB {
		return db.Order("created_at ASC")
	})
}
